import React from 'react'
import { Input } from 'baseui/input'
import { FormControl } from 'baseui/form-control'
import { Button } from 'baseui/button'

const App = () => {
  return (
    <form>
      <div className="w-2/5 m-auto mt-10">
        <FormControl label={() => 'Name'}>
          <Input name="name" placeholder="Eg: Mukesh" />
        </FormControl>

        <FormControl label={() => 'Year of Birth'}>
          <Input name="year" placeholder="Eg: 1985" />
        </FormControl>

        <FormControl label={() => 'Your favourite subject'}>
          <Input name="subject" placeholder="Eg: English" />
        </FormControl>

        <FormControl label={() => 'Your favourite romatic movie'}>
          <Input name="movie" placeholder="Eg: Titanic" />
        </FormControl>

        <FormControl label={() => 'Your role model'}>
          <Input name="model" placeholder="Eg: A. P. J. Abdul Kalam" />
        </FormControl>

        <Button>Submit</Button>
      </div>
    </form>
  )
}

export default App
